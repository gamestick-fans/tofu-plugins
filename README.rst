**************************
TOFU plugins for GameStick
**************************

TOFU was the XBMC 12 (Kodi) clone that was officially available on the
PlayJam GameStick.

It has a plugin interface that allows us to do things on the stick that
PlayJam did not want us to do.


.. contents::


================
Addon repository
================
Installation of these addons on a GameStick is much easier if you
override the DNS entry of ``pivos-addons.pivoscdn.com``
to the IP address ``178.254.13.17``.

This server contains a plugin repository that allows easy installation
of all addons without copying them to SD card.


================
Available addons
================

script.copygamestickcache
=========================
Copies all cache data from ``/data/GameStickCache`` onto the SD card.

This allows us to obtain information about the games that once were
available on the GameStick server.


Usage
-----
1. Download the zip file ``script.copygamestickcache-?.?.?.zip``
   (from the Codeberg `releases section`__) and store it on the SD card
2. Plug SD card into gamestick
3. Start Tofu > System > Addons > Install from zip > external storage
4. Tofu main menu > Programs > Program addons > Copy GameStick cache
5. A notification will pop up: "Starting to copy"
6. It will take 30+ seconds to copy. A notification will pop up:
   "Finished copying. Exit code: xxx"
7. Unplug the SD card and send the files ``profile_response.json``
   and ``reg_server_response.json`` to cweiske


__ https://codeberg.org/gamestick-fans/tofu-plugins/releases


script.start-adbd
=================
Note: It's easier to flash a firmware patch that permanently enables ``adbd``
- see https://codeberg.org/gamestick-fans/firmware-adb-enabler .

Starts the Android Debug Bridge Daemon (adbd) on the GameStick.

Once started, you can connect over network to the GameStick with adb::

  $ adb connect gamestick
  connected to gamestick:5555
  $ adb shell
  root@android:/ #


Usage
-----
1. Download the zip file ``script.start-adbd-?.?.?.zip``
   (from the Codeberg `releases section`__) and store it on the SD card
2. Plug SD card into gamestick
3. Start Tofu > System > Addons > Install from zip > external storage
4. Tofu main menu > Programs > Program addons > Start adbd
5. A notification will pop up: "Successfully started adbd"


__ https://codeberg.org/gamestick-fans/tofu-plugins/releases



script.telnet-server
====================
Starts a telnet server that provides a crude shell on the GameStick.

Features:

- changing directories with ``cd``
- run programs like ``ls`` and ``su``

Known problems:

- Program output is not sent immediately to the client, only when
  the execution finished.
- Some program invocations cause the telnet server to hang,
  and you have to reboot the GameStick to get it working again.
- The ``cd`` path is shared across all connections :)


Usage
-----
1. Download the zip file ``script.telnet-server-?.?.?.zip``
   (from the Codeberg `releases section`__) and store it on the SD card
2. Plug SD card into gamestick
3. Start Tofu > System > Addons > Install from zip > external storage
4. Tofu main menu > Programs > Program addons > Telnet server
5. A notification will pop up: "Starting server on port 5023"
6. On your computer, connect to the gamestick::

     $ telnet gamestickip 5023


__ https://codeberg.org/gamestick-fans/tofu-plugins/releases

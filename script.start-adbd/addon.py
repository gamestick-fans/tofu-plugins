"""
    Plugin to start adbd on the GameStick
"""
import os
import subprocess
import sys
import xbmc

try:
    process = subprocess.Popen(
        ["ps", "-C", "adbd"],
        stderr = subprocess.STDOUT,
        stdout = subprocess.PIPE
    )
    stdout, stderr = process.communicate()
except BaseException as ex:
    xbmc.executebuiltin("XBMC.Notification(Failed to check if adbd is running, %s, 2000)" % str(ex))
    sys.exit(1)

lines = stdout.splitlines()
if len(lines) > 1:
    xbmc.executebuiltin("XBMC.Notification(Start adbd, adbd is already running, 5000)")
    sys.exit(0)


exitcode = os.system("su -c start adbd")
if exitcode == 0:
    xbmc.executebuiltin("XBMC.Notification(Start adbd, Successfully started adbd., 2000)")
else:
    xbmc.executebuiltin("XBMC.Notification(Start adbd, Failed to start adbd. Exit code: %s, 2000)" % exitcode)

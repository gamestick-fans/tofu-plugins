"""
    Plugin to run a simple telnet server
"""
import xbmc

xbmc.executebuiltin("XBMC.Notification(Telnet server, Starting server on port 5023, 5000)")
import telnetserver
xbmc.executebuiltin("XBMC.Notification(Telnet server, Server stopped, 5000)")

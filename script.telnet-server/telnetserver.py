# Socket server in python using select function
# taken from
# https://www.binarytides.com/python-socket-server-code-example/

import re, os, socket, select, sys, subprocess, shlex

CONNECTION_LIST = []    # list of socket clients
RECV_BUFFER = 4096 # Advisable to keep it as an exponent of 2
PORT = 5023 #telnet

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# this has no effect, why ?
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(("0.0.0.0", PORT))
server_socket.listen(10)

# Add server socket to the list of readable connections
CONNECTION_LIST.append(server_socket)

print "Chat server started on port " + str(PORT)
shutdown = False
cwd = os.getcwd()

while 1:
    # Get the list sockets which are ready to be read through select
    read_sockets,write_sockets,error_sockets = select.select(CONNECTION_LIST, [], [])
    for sock in read_sockets:
        #New connection
        if sock == server_socket:
            # Handle the case in which there is a new connection recieved through server_socket
            sockfd, addr = server_socket.accept()
            CONNECTION_LIST.append(sockfd)
            print "Client (%s, %s) connected" % addr
            sockfd.send(cwd + "> ")

            #Some incoming message from a client
        else:
            # Data recieved from client, process it
            try:
                #In Windows, sometimes when a TCP program closes abruptly,
                # a "Connection reset by peer" exception will be thrown
                data = sock.recv(RECV_BUFFER)

                #remove newline
                data = data.rstrip()
                # echo back the client message
                #print("len: " + str(len(data)))
                if len(data):
                    if data == "shutdown":
                        shutdown = True
                        break

                    elif data == "quit" or data == '\xff\xfb\06':
                        #ctrl+c
                        sock.close()
                        CONNECTION_LIST.remove(sock)
                        break

                    elif data[:3] == "cd ":
                        newcwd = data[3:]
                        if newcwd[0] == "/":
                            cwd = newcwd
                        else:
                            cwd = cwd + "/" + newcwd
                            cwd = re.sub(r'/[^/]+/\.\.', '', cwd)
                            cwd = re.sub(r'//', '/', cwd)

                    else:
                        try:
                            args = shlex.split(data)
                            process = subprocess.Popen(
                                args,
                                stderr = subprocess.STDOUT,
                                stdout = subprocess.PIPE,
                                cwd = cwd
                            )
                            stdout, stderr = process.communicate()
                            sock.send(stdout)
                        except BaseException as ex:
                            sock.send(str(ex) + "\n")

                sock.send(cwd + "> ")
                # client disconnected, so remove from socket list
            except:
                print(sys.exc_info())
                print "Client (%s, %s) is offline" % addr
                sock.close()
                CONNECTION_LIST.remove(sock)
                continue

    if shutdown == True:
        print("Shutting down\n");
        break

server_socket.close()

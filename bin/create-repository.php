#!/usr/bin/env php
<?php
/**
 * Create an Kodi/XBMC addon repository from dist/
 * https://kodi.wiki/view/Add-on_repositories
 */

$addonsZipDir = dirname(__FILE__, 2) . '/dist/';
$repoDir = dirname(__FILE__, 2) . '/dist/repo/';

if (!is_dir($repoDir)) {
    mkdir($repoDir);
}

$latest = [];
foreach (glob($addonsZipDir . '*.zip') as $addonsFile) {
    $file  = basename($addonsFile, '.zip');
    $parts = explode('-', $file);
    $version = array_pop($parts);
    $addonId = implode('-', $parts);
    if (!isset($latest[$addonId])) {
        $latest[$addonId] = (object) [
            'id'      => $addonId,
            'file'    => $addonsFile,
            'version' => $version,
        ];
    } else {
        $lastVersion = $latest[$addonId]->version;
        if (version_compare($version, $lastVersion, '>')) {
            $latest[$addonId] = (object) [
                'id'      => $addonId,
                'file'    => $addonsFile,
                'version' => $version,
            ];
        }
    }
}

$xmls = [];
foreach ($latest as $addonInfo) {
    $zip = new ZipArchive();
    $res = $zip->open($addonInfo->file, ZipArchive::RDONLY);
    if ($res !== true) {
        fwrite(STDERR, 'Could not open zip file: ' . $addonInfo->file . "\n");
        exit(10);
    }

    $addonId = $addonInfo->id;
    $xml = $zip->getFromName($addonId . '/addon.xml');
    if ($xml === false) {
        fwrite(STDERR, 'Could not extract addon.xml from ' . $addonInfo->file . "\n");
        exit(10);
    }
    $xmls[$addonId] = $xml;

    $addonDir = $repoDir . $addonId . '/';
    if (!is_dir($addonDir)) {
        mkdir($addonDir);
    }

    copy($addonInfo->file, $addonDir . basename($addonInfo->file));

    $filesToExtract = [
        $addonId . '/fanart.jpg',
        $addonId . '/icon.png',
    ];
    foreach ($filesToExtract as $filePath) {
        if ($zip->locateName($filePath) === false) {
            continue;
        }
        $fileContent = $zip->getFromName($filePath);
        if ($fileContent === false) {
            fwrite(
                STDERR,
                'Could not extract ' . $filePath . ' from ' . $addonInfo->file . "\n"
            );
            exit(10);
        }
        file_put_contents($addonDir . basename($filePath), $fileContent);
    }
}

$domAddons = (new DOMImplementation())->createDocument(null, 'addons');
$domAddons->preserveWhiteSpace = false;
$domAddons->formatOutput = true;
foreach ($xmls as $addonXml) {
    $domAddon = new DOMDocument();
    $domAddon->preserveWhiteSpace = false;
    $domAddon->formatOutput = true;
    $domAddon->loadXML($addonXml);

    $imported = $domAddons->importNode($domAddon->documentElement, true);
    $domAddons->documentElement->appendChild($imported);
}

$domAddons->save($repoDir . 'addons.xml');

file_put_contents($repoDir . 'addons.xml.md5', date('c'));

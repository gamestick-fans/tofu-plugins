﻿"""
    Plugin to copy GameStick cache files to SD card
"""
import os
import sys
import xbmc

#print "test cweiske"
sdCardPath = "/storage/sdcard0/external_sdcard/"
if not os.path.exists(sdCardPath):
    xbmc.executebuiltin("XBMC.Notification(Copy GameStick cache, SD card missing, 5000)")
    exit(1)

xbmc.executebuiltin("XBMC.Notification(Copy GameStick cache, Starting to copy, 2000)")
exitcode = os.system("cp -r /data/GameStickCache %s" % sdCardPath)
xbmc.executebuiltin("XBMC.Notification(Copy GameStick cache, Finished copying. Exit code: %s, 2000)" % exitcode)
